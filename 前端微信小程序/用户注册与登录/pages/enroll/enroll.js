// pages/enroll/enroll.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: "",
    phonenumber: "",
    password: "",
    passwordack:"",
  },

  /**
    * 获取验证码
    */
  //  doGetCode: function () {
  //   var that = this;
  //   that.setData({
  //     disabled: true, //只要点击了按钮就让按钮禁用 （避免正常情况下多次触发定时器事件）
  //     color: '#ccc',
  //   })
 
  //   var phone = that.data.phone;
  //   var currentTime = that.data.currentTime //把手机号跟倒计时值变例成js值
  //   var warn = null; //warn为当手机号为空或格式不正确时提示用户的文字，默认为空
  //   var phone = that.data.phone;
  //   var currentTime = that.data.currentTime //把手机号跟倒计时值变例成js值
  //   var warn = null; //warn为当手机号为空或格式不正确时提示用户的文字，默认为空
  //   wx.request({
  //     url: '', //后端判断是否已被注册， 已被注册返回1 ，未被注册返回0
  //     method: "GET",
  //     header: {
  //       'content-type': 'application/x-www-form-urlencoded'
  //     },
  //     success: function (res) {
  //       that.setData({
  //         state: res.data
  //       })
  //       if (phone == '') {
  //         warn = "号码不能为空";
  //       } else if (phone.trim().length != 11 || !/^1[3|4|5|6|7|8|9]\d{9}$/.test(phone)) {
  //         warn = "手机号格式不正确";
  //       } //手机号已被注册提示信息
  //        else if (that.data.state == 1) {  //判断是否被注册
  //          warn = "手机号已被注册";
 
  //        }
  //        else {
  //         wx.request({
  //           url: '', //填写发送验证码接口
  //           method: "POST",
  //           data: {
  //             coachid: that.data.phone
  //           },
  //           header: {
  //             'content-type': 'application/x-www-form-urlencoded'
  //           },
  //           success: function (res) {
  //             console.log(res.data)
  //             that.setData({
  //              VerificationCode: res.data.verifycode
  //             })

  //             //当手机号正确的时候提示用户短信验证码已经发送
  //             wx.showToast({
  //               title: '短信验证码已发送',
  //               icon: 'none',
  //               duration: 2000
  //             });
  //             //设置一分钟的倒计时
  //             var interval = setInterval(function () {
  //               currentTime--; //每执行一次让倒计时秒数减一
  //               that.setData({
  //                 text: currentTime + 's', //按钮文字变成倒计时对应秒数
 
  //               })
  //               //如果当秒数小于等于0时 停止计时器 且按钮文字变成重新发送 且按钮变成可用状态 倒计时的秒数也要恢复成默认秒数 即让获取验证码的按钮恢复到初始化状态只改变按钮文字
  //               if (currentTime <= 0) {
  //                 clearInterval(interval)
  //                 that.setData({
  //                   text: '重新发送',
  //                   currentTime: 61,
  //                   disabled: false,
  //                   color: '#33FF99'
  //                 })
  //               }
  //             }, 100);
  //           }
  //         })
  //       };
  //       //判断 当提示错误信息文字不为空 即手机号输入有问题时提示用户错误信息 并且提示完之后一定要让按钮为可用状态 因为点击按钮时设置了只要点击了按钮就让按钮禁用的情况
  //       if (warn != null) {
  //         wx.showModal({
  //           title: '提示',
  //           content: warn
  //         })
  //         that.setData({
  //           disabled: false,
  //           color: '#33FF99'
  //         })
  //         return;
  //       }
  //     }
 
  //   })
 
  // },

  signin: function (e) {
    // 在B页面内 navigateBack，将返回A页面
    wx.navigateBack({
      delta: 1
      // delta: 1表示跳转到上1页，在此上一页为登陆页面
    })
  },
  regist: function (e) {
    var that = this
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    // 正则表达式，表示手机号13或15或18或17开头，后8位数字可任意，用以校验手机号输入是否合法
    // console.log("nsvcjcndsncs")用以查看函数是否成功被运行了
    // 第一部分：判断各输入框值是否有输入
    if (that.data.username == ""&&that.data.phonenumber=="") {
      wx.showModal({
        title: '提示',
        content: '请输入用户名或手机号',
        showCancel: false,
        success(res) {
          // if (res.confirm) {
          //   console.log('用户点击确定')
          // } else if (res.cancel) {
          //   console.log('用户点击取消')
          // }
        }
      })
    }
    // else if(that.data.phonenumber==""){
    //   wx.showModal({
    //     title: '提示',
    //     content: '请输入手机号',
    //     showCancel: false,
    //     success(res) { }
    //   })
    // }
     // 第二部分：判断输入框中内容是否合法
     else if (that.data.phonenumber!=""&&that.data.phonenumber.length != 11) {
      wx.showModal({
        title: '提示',
        content: '手机号长度有误，请重新输入',
        showCancel: false,
        success(res) { }
      })
    }
    else if (that.data.phonenumber!=""&&!myreg.test(that.data.phonenumber)) {
      wx.showModal({
        title: '提示',
        content: '请输入正确的手机号码',
        showCancel: false,
        success(res) { }
      })
    }
    // if (this.data.Code == '') {
    //   wx.showToast({
    //     title: '提示',
    //     content: '请输入验证码',
    //     showCancel: false,
    //     success(res) { }
    //   })
    //   return
    // } else if (this.data.Code != this.data.VerificationCode) {
    //   wx.showToast({
    //     title: '提醒',
    //     content:'验证码错误',
    //    // image: '/images/error.png',
    //     showcancel:false,
    //     success(res){}
    //   })
    //   return
    // }
    else if (that.data.password == "") {
      wx.showModal({
        title: '提示',
        content: '请输入密码',
        showCancel: false,
        success(res) { }
      })
    }
    else if (that.data.passwordack == "") {
      wx.showModal({
        title: '提示',
        content: '请输入确认密码',
        showCancel: false,
        success(res) { }
      })
    }
   else if (that.data.password !=that.data.passwordack ) {
      wx.showModal({
        title: '提示',
        content: '两次输入密码不一致',
        showCancel: false,
        success(res) { }
      })
    }
  },
  usernameInput: function (e) {
    this.data.username = e.detail.value
    // console.log( this.data.username)用调试窗口以验证是否成功
    // e.detail.value获取当前输入框的值
  },
  phonenumberInput: function (e) {
    this.data.phonenumber = e.detail.value
  },
  passwordInput: function (e) {
    this.data.password = e.detail.value
  },
  passwordAckInput: function (e) {
    this.data.passwordack = e.detail.value
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})