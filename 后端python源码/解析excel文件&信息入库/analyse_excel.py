# -*- coding: UTF-8 -*-
import pandas as pd
from math import isnan
import json
import os


class DealExcel():
    '''
    v2版本，将excel转化为json并且存入当前工作目录,此模块无需单独运行，为up_to_db.py模块
    Version 2 convert excel to json , saved on current path,this module is supported to up_to_db.py, need not run alone
    该类的作用为分析指定文件目录下的excel文件，整理，提取，保存为json文件
    Class DealExcel is build for analyse some excels where designated file path,save as json
    该类接收一个实参代表excel的文件路径
    DealExcel accept a argument as excel_files store path
    输出json文件默认在当前路径下，如果在函数save_json()输入1个参数则为指定输出路径，2个参数则分别是输入路径文件夹，输出路径
    output json files are default saved current path,and save on designated path if has path argument:1 argument is accpet input file,
    2 arguments means input and output path
    '''

    # 获取excel文件地址，deal_excel类接收一个文件地址参数
    def __init__(self, path):
        self.path = path

    # 读取excel文件并分析数据,返回值为嵌套着字典的列表
    def read_excel(self):
        pwd = os.path.dirname(os.path.abspath(__file__))
        os.chdir(pwd)  # 切换到当前工作路径
        workbook = pd.read_excel(self.path, index=None)
        nrows = workbook.index.size  # 行数
        ncols = workbook.columns.size  # 列数

        test = []
        for i in range(nrows):
            tmp = {}
            for j in range(ncols):
                dict_key = workbook.iloc[:, j].name  # 获取当前列字段名
                dict_value = workbook.iloc[i][j]  # 当前遍历单元格值
                tmp[dict_key] = dict_value
            test.append(tmp)

        test = list(test)
        return test

    # 以json的格式保存处理完的excel数据，参数为空
    @staticmethod
    def save_json(*arg_path):
        if len(arg_path) == 0:  # 无参数时，读取file文件夹下excel，输出同级目录文件
            file_path = os.path.dirname(os.path.abspath(__file__)) + '\\file'  # 默认定位到从file文件夹（读取excel文件）
            out_path = os.path.dirname(os.path.abspath(__file__))
        elif len(arg_path) == 1:  # 一个参数时，意义为指定输出的文件路径
            file_path = os.path.dirname(os.path.abspath(__file__)) + '\\file'
            out_path = arg_path[0]  # 定位到第一个参数指定的路径
        elif len(arg_path) == 2:  # 二个参数时，分别表示 指定输入和输出文件的路径
            file_path = arg_path[0]
            out_path = arg_path[1]

        files = os.listdir(file_path)  # 列出输入路径中所有的excel文件
        for file in files:
            os.chdir(out_path)  # 切换输出路径
            if os.path.splitext(file)[1] == '.xlsx' or os.path.splitext(file)[1] == '.xls': # 只处理excel文件类型
                tmp_demo = DealExcel(file_path + '\\' + file)  # tmp_demo是一个定义一个临时的类，迭代不同的excel
                json_out = os.path.splitext(file)[0] + '.json'  # json_out获取excel文件名作为json文件命名
                with open(json_out, 'w', encoding='utf-8') as f_obj:
                    # print(json_out, '\t', tmp_demo.read_excel(), '\n')  # 测试函数
                    json.dump(tmp_demo.read_excel(), f_obj, indent=4, ensure_ascii=False)  # json的输出内容


if __name__ == "__main__":
    exe_excel = DealExcel('')  # 可以留空字符串
    exe_excel.save_json()  # 'D:\\pycharm\\analyse_excel\\excel_test','D:\\pycharm\\analyse_excel'  [usage：输入路径，输出路径]
    print(exe_excel.__doc__)